Cabot IRC Plugin
===

This alert plugin will send a notification on an IRC channel.

# Installation

    $ pip install cabot-alert-irc

Add `cabort_alert_irc` to the installed apps in settings.py.

    $ foreman run python manage.py syncdb --migrate
    $ foreman start

# Configuration

Provide the following variable in your `<file>.env` (or uses the following default):

    IRC_HOST=sinisalo.freenode.net
    IRC_PORT=6667
    IRC_BOT_NICK=Cabot_Alert_Bot
    IRC_ROOM=Cabot_Alert_Bot_Test

I suggest you change the `IRC_BOT_NICK` with something unique.
